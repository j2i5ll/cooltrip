export default {
  appBar: {
    backgroundColor: '#dc143c',
    boxShadow: 'none'
  },
  header: {
    position: 'fixed',
    top: '0',
    width: '100%',
    zIndex: 100
  },
  code: {
    height: '2em',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#722F37',
    color: '#ffffff',
    boxShadow: 'rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px'
  },
  main: {
    backgroundColor: '#e8efe1',
    height: '100%'
  },
  orderResult: {
    paddingTop: '100px'
  },
  orderTitle: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  titleIcon: {
    marginRight: '0.3em',
    fontSize: '2em'
  },
  title: {
    textAlign: 'center',
    color: '#33ccff',
    width: '100%'
  },
  total: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  orderResultFooter: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  buttonContainer: {
    paddingTop: '2em',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'

  },
  comment: {
    paddingTop: '1.5em',
    textAlign: 'center'
  },
  adContainer: {
    position: 'fixed',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 0,
    width: '100%',
    paddingBottom: '2em'
  },
  ad: {
    width: '80%',
    backgroundColor: 'white',
    height: '3em',
    border: '1px solid black',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  dummyFooter: {
    height: '5em'
  },
  btn: {
    backgroundColor: '#722F37'
  }
}
