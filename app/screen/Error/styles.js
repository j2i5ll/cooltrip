export default {
  main: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    right: 0,
    top: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: '100%',
    flexDirection: 'column',
    backgroundColor: '#e8efe1'
  },
  appBar: {
    backgroundColor: '#dc143c'
  },
  header: {
    position: 'fixed',
    top: '0',
    width: '100%',
    zIndex: 100
  },
  body: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  title: {
    textAlign: 'center',
    color: '#33ccff',
    width: '100%'
  },
  btn: {
    backgroundColor: '#722F37'
  }

}
