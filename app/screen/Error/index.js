import React, { Component } from 'react'
import IconButton from 'material-ui/IconButton'
import RaisedButton from 'material-ui/RaisedButton'
import FQIcon from '../../components/FQIcon'
import styles from './styles.js'
import font from '../../style/font.js'
import {connect} from 'react-redux'
import AppBar from 'material-ui/AppBar'
import ActionHome from 'material-ui/svg-icons/action/home'
import { withRouter } from 'react-router'

class Error extends Component {
  componentDidMount () {
    window.scrollTo(0, 0)
  }
  render () {
    const {history} = this.props
    const langCode = this.props.language.code
    return (
      <div style={styles.main} >
        <div style={styles.header} >
          <AppBar
            style={styles.appBar}
            title={(<div style={{...styles.title, ...font.preset4}} >The CoolTE</div>)}
            iconElementLeft={<FQIcon onClick={() => this.props.history.push(`/${langCode}/help`)} />}
            iconElementRight={<IconButton><ActionHome /></IconButton>}
            onRightIconButtonTouchTap={() => this.props.history.push(`/${langCode}`)}
          />
        </div>
        <div style={styles.body} >
          <h3>Cannot found your request.</h3>
          <br/>
          <RaisedButton buttonStyle={styles.btn} labelStyle={{color: '#ffffff'}} label={'Home'} onClick={() => history.push(`/${langCode}`)} />
        </div>
      </div>
    )
  }
}
function mapStateToProps (state) {
  const {
    language
  } = state
  return {language}
}
export default connect(mapStateToProps)(withRouter(Error))
