import React, { Component } from 'react'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import TextField from 'material-ui/TextField'
import Footer from '../../components/Footer'
import RaisedButton from 'material-ui/RaisedButton'
import FQIcon from '../../components/FQIcon'
import AppBar from 'material-ui/AppBar'
import {setLanguage} from 'redux-i18n'

import styles from './styles.js'

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as languageAction from '../../actions/language'
import * as orderAction from '../../actions/order'
import PropTypes from 'prop-types'

class Main extends Component {
  constructor () {
    super()
    this.availHeight = window.innerHeight
    this.state = {
      langIndex: 0,
      code: ''
    }
    this.search = this.search.bind(this)
    this.changeLanguage = this.changeLanguage.bind(this)
  }
  search (e) {
    e.preventDefault()
    const langCode = this.props.language.code

    if (this.state.code === '') {
      this.setState({
        errorText: 'Please enter the code'
      })
      return false
    }
    this.props.history.push({
      pathname: `/${langCode}/restaurant`,
      search: '?code=' + this.state.code
    })
  }
  changeLanguage (event, index, code) {
    this.props.changeLanguage(code)
    this.props.setLanguage(code)
  }
  componentDidMount () {
    this.props.initOrder()
    // onLeftIconButtonTouchTap={() => this.props.history.push(`/${langCode}/help`)}
  }
  render () {
    const trans = this.context.t
    const langCode = this.props.language.code
    // iconElementLeft={<IconButton><FontIcon className='material-icons' color={'#000000'} >live_help</FontIcon></IconButton>}
    return (
      <div style={{...styles.main, minHeight: this.availHeight + 'px'}} >
          <AppBar
            style={styles.header}
            title={<img src='' />}
            iconElementLeft={<FQIcon onClick={() => this.props.history.push(`/${langCode}/help`)} />}
          />
        <div style={styles.title} >
          <h5 style={styles.beta} >beta</h5>
          <h2 style={styles.mainTitle} >{trans('The Coolest Trip Ever')}</h2>
          <h4 style={styles.intro} >{trans('A Foodie`s Travel Guide')}</h4>
        </div>
        <div style={styles.search} >
          <h4 style={styles.typeCode} >{trans('Type a restaurant code')}</h4>
          <form style={styles.form} onSubmit={this.search} >
            <TextField floatingLabelText='Restaurant code' errorText={this.state.errorText} onChange={(event, code) => this.setState({code})} />
            <RaisedButton type='submit' label={trans('Send')} onClick={this.search} style={styles.searchBtn} />
          </form>
        </div>
        <div style={styles.lang} >
          <h4 style={styles.typeCode} >{trans('Select language')}</h4>
          <SelectField value={this.props.language.code} onChange={this.changeLanguage} floatingLabelText="Language" >
            <MenuItem value={'en'} primaryText='English' />
            <MenuItem value={'es'} primaryText='Español' />
            <MenuItem value={'jp'} primaryText='日本' />
            <MenuItem value={'cs'} primaryText='简体中文' />
            <MenuItem value={'ct'} primaryText='繁体中文' />
            <MenuItem value={'it'} primaryText='italiano' />
            <MenuItem value={'de'} primaryText='Deutsch' />
            <MenuItem value={'ru'} primaryText='русский' />
            <MenuItem value={'fr'} primaryText='Français' />
            <MenuItem value={'th'} primaryText='ไทย' />
            <MenuItem value={'vn'} primaryText='Tiếng Việt' />
          </SelectField >
        </div>
        <div style={styles.ad} >
          <h3>AD area</h3>
        </div>
        <Footer />
      </div>
    )
  }
}
Main.contextTypes = {
  t: PropTypes.func
}
function mapStateToProps (state) {
  const {
    language
  } = state
  return {language}
}
function mapDispatchToProps (dispatch) {
  return bindActionCreators({...orderAction, ...languageAction, setLanguage}, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Main)
