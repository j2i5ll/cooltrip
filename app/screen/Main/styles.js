export default {
  main: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'column',
    paddingBottom: '4em',
    color: '#ffffff',
    backgroundColor: '#dc143c'
  },
  header: {
    position: 'absolute',
    top: '0',
    backgroundColor: 'transparent',
    boxShadow: 'none'
  },
  title: {
    textAlign: 'center',
    paddingTop: '2em'
  },
  mainTitle: {
    color: '#33ccff',
    fontFamily: 'Josefin Sans, sans-serif'
  },
  beta: {
    fontStyle: 'italic',
    color: '#33ccff',
    fontFamily: 'Josefin Sans, sans-serif'
  },
  typeCode: {
    fontFamily: 'Libre Franklin, sans-serif'
  },
  intro: {
    fontFamily: 'Architects Daughter, cursive'
  },
  search: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  searchBtn: {
    marginTop: '1em'
  },
  lang: {
  },
  ad: {
    textAlign: 'center',
    width: '80%',
    height: '3em',
    border: '1px solid black',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
}
