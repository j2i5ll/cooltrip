export default {
  main: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: '100%',
    backgroundColor: '#e8efe1',
    flexDirection: 'column'
  },
  appBar: {
    backgroundColor: '#dc143c',
    boxShadow: 'none'
  },
  header: {
    position: 'fixed',
    top: '0',
    width: '100%',
    zIndex: 100
  },
  restaurant: {
    width: '100%',
    height: '100%',
    paddingTop: '100px'
  },
  code: {
    height: '2em',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#722F37',
    color: '#ffffff',
    boxShadow: 'rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px'
  },
  restaurantInfo: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'column',
    paddingBottom: '7.5em'
  },
  restaurantName: {
    textAlign: 'center',
    paddingTop: '10px',
    paddingBottom: '20px'
  },
  translatedName: {
    color: '#888888'
  },
  adContainer: {
    position: 'fixed',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    bottom: '4.5em',
    width: '100%',
    zIndex: 5
  },
  ad: {
    width: '80%',
    backgroundColor: 'white',
    height: '3em',
    border: '1px solid black',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    textAlign: 'center',
    width: '100%',
    color: '#33ccff'
  }

}
