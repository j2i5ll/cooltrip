import React, { Component } from 'react'
import FQIcon from '../../components/FQIcon'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as restaurantActions from '../../actions/restaurant'
import * as orderActions from '../../actions/order'
import IconButton from 'material-ui/IconButton'

import styles from './styles.js'
import font from '../../style/font.js'
import AppBar from 'material-ui/AppBar'
import ActionHome from 'material-ui/svg-icons/action/home'
import Loading from '../../components/Loading'
import OrderState from '../../components/OrderState'
import MenuCard from '../../components/MenuCard'

class Restaurant extends Component {
  componentWillMount () {
    const search = new URLSearchParams(this.props.location.search)
    const query = this.props.location.query
    const langCode = this.props.language.code
    if (search.get('code') !== null) {
      this.props.fetchRestaurant(search.get('code'))
    } else if (query !== undefined && query.code !== undefined) {
      this.props.fetchRestaurant(query.code)
    } else {
      this.props.history.push(`/${langCode}`)
    }
  }
  componentDidMount () {
    window.scrollTo(0, 0)
  }
  getLangTitle () {
    const {restaurant} = this.props
    const {defaultLang} = restaurant
    if (restaurant.name) {
      return defaultLang === 'ko' ? restaurant.name.origin : restaurant.name.translate[defaultLang]
    } else {
      return ''
    }
  }
  render () {
    const menus = this.props.restaurant.menu
    const menuList = menus.map((item, index) => {
      return (<MenuCard menuItem={item} key={index}/>)
    })
    const {restaurant} = this.props
    const langCode = this.props.language.code
    return (
      <div style={{...styles.main, ...font.preset5}} >
        <Loading />
        <div style={styles.restaurant}>
          <div style={styles.header} >
            <AppBar
              style={styles.appBar}
              title={(<div style={{...styles.title, ...font.preset4}} >The CoolTE</div>)}
              iconElementLeft={<FQIcon onClick={() => this.props.history.push(`/${langCode}/help`)} />}
              iconElementRight={<IconButton><ActionHome /></IconButton>}
              onRightIconButtonTouchTap={() => this.props.history.push(`/${langCode}`)}
            />
            <div style={{...styles.code, ...font.preset5}} >
              <div>{restaurant.code}</div>
            </div>
          </div>
          <div style={styles.restaurantInfo} >
            <div style={styles.restaurantName}>
              <h1 style={font.preset1} >{this.getLangTitle()}</h1>
              <h3 style={{...styles.translatedName, ...font.preset5}} >{restaurant.name && restaurant.name.translate[this.props.language.code]}</h3>
            </div>
            {menuList}
          </div>
          <OrderState />
          <div style={styles.adContainer} >
            <div style={styles.ad} >
              <span>AD Area</span>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
function mapStateToProps (state) {
  return {
    isFetching: state.restaurant.isFetching,
    restaurant: state.restaurant.data,
    language: state.language
  }
}
function mapDispatchToProps (dispatch) {
  return bindActionCreators({...restaurantActions, ...orderActions}, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Restaurant)
