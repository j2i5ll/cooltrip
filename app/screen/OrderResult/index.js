import React, { Component } from 'react'
import FQIcon from '../../components/FQIcon'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import { withRouter } from 'react-router'
import * as restaurantActions from '../../actions/restaurant'
import * as orderActions from '../../actions/order'
import ActionHome from 'material-ui/svg-icons/action/home'
import AppBar from 'material-ui/AppBar'
import IconButton from 'material-ui/IconButton'
import RaisedButton from 'material-ui/RaisedButton'
import FontIcon from 'material-ui/FontIcon'
import OrderItem from '../../components/OrderItem'
import Divider from 'material-ui/Divider'
import {List, ListItem} from 'material-ui/List'
import styles from './styles.js'
import Utils from '../../libs/Utils'
import font from '../../style/font'

class OrderResult extends Component {
  getOrderedMenu () {
    return this.props.order.menu.map((item, index) => {
      return (<OrderItem orderedMenu={item} index={index} key={index} />)
    })
  }
  componentDidMount () {
    window.scrollTo(0, 0)
  }
  render () {
    const orderedMenus = this.props.order.menu
    const langCode = this.props.language.code

    let sumCount = 0
    let sumPrice = 0
    for (let i = 0; i < orderedMenus.length; i++) {
      const menuItem = this.props.restaurant.menu.find((item) => item.id === orderedMenus[i].id)
      if (typeof orderedMenus[i].count === 'number') {
        sumCount += orderedMenus[i].count
        sumPrice += (orderedMenus[i].count * menuItem.price)
      } else {
        for (let j = 0; j < orderedMenus[i].count.length; j++) {
          const itemPrice = menuItem.price.find((item) => item.size === orderedMenus[i].count[j].size)
          sumCount += orderedMenus[i].count[j].count
          sumPrice += (orderedMenus[i].count[j].count * itemPrice.price)
        }
      }
    }
    sumPrice = isNaN(sumPrice) ? 'ask to the server' : Utils.numberToPrice(sumPrice)

    return (
      <div style={styles.main} >
        <div style={styles.header} >
          <AppBar
            style={styles.appBar}
            title={(<div style={{...styles.title, ...font.preset4}} >The CoolTE</div>)}
            iconElementLeft={<FQIcon onClick={() => this.props.history.push(`/${langCode}/help`)} />}
            iconElementRight={<IconButton><ActionHome /></IconButton>}
            onRightIconButtonTouchTap={() => this.props.history.push(`/${langCode}`)}
          />
          <div style={{...styles.code, ...font.preset5}} >
            <div>{this.props.restaurant.code}</div>
          </div>
        </div>
        <div style={styles.orderResult} >
          <List>
            <ListItem primaryText={(
              <div style={styles.orderTitle} >
                <FontIcon className='material-icons' style={styles.titleIcon} >playlist_add_check</FontIcon>
                <h2 style={font.preset6} >You will order...</h2>
              </div>
            )} />
            <Divider/>
          </List>
          {this.getOrderedMenu()}
          <List>
            <ListItem primaryText={(
              <div style={styles.orderResultFooter} >
                <div style={styles.total} >
                  <h4 style={font.preset6} >Total</h4>
                  <h4 style={font.preset1} >{sumCount} items -  &#8361;{sumPrice}</h4>
                </div>
                <div style={styles.buttonContainer} >
                  <RaisedButton label={'Back'} labelStyle={{...font.preset6, color: '#ffffff'}} buttonStyle={styles.btn} onClick={() => this.props.history.goBack()}/>
                  <RaisedButton labelStyle={{...font.preset6, color: '#ffffff'}} buttonStyle={styles.btn} label={'OK'} onClick={() => this.props.history.push(`/${langCode}/confirm`)} />
                </div>
              </div>
            )} />
          </List>
        </div>
        <div style={styles.dummyFooter} ></div>
        <div style={styles.adContainer} >
          <div style={styles.ad} >
            <span>AD Area</span>
          </div>
        </div>
      </div>
    )
  }
}
function mapStateToProps (state) {
  const {
    order,
    language
  } = state
  return {
    order,
    restaurant: state.restaurant.data,
    language
  }
}
function mapDispatchToProps (dispatch) {
  return bindActionCreators({...restaurantActions, ...orderActions}, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(OrderResult))
