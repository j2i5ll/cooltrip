import React, { Component } from 'react'
import FQIcon from '../../components/FQIcon'
import IconButton from 'material-ui/IconButton'
import {connect} from 'react-redux'
import styles from './styles.js'
import AppBar from 'material-ui/AppBar'
import ActionHome from 'material-ui/svg-icons/action/home'
import Divider from 'material-ui/Divider'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import InpterpreterCard from '../../components/InterpreterCard'
import fetch from 'cross-fetch'
class Help extends Component {
  constructor () {
    super()
    this.state = {
      left: 'en',
      right: 'ko',
      list: []
    }
    this.changeLeft = this.changeLeft.bind(this)
    this.changeRight = this.changeRight.bind(this)
    this.getInterpreter = this.getInterpreter.bind(this)
  }
  changeLeft (event, index, code) {
  /*
    console.log(code)
    this.setState({
      left: code
    })
    */
    this.getInterpreter(code, this.state.right)
  }
  changeRight (event, index, code) {
  /*
    this.setState({
      right: code
    })
    */
    this.getInterpreter(this.state.left, code)
  }
  getInterpreter (left, right) {
    fetch(`/api/fq?a=${left}&b=${right}`).then(res => res.json())
    .then(json => {
      this.setState({
        left: left,
        right: right,
        list: json
      })
    })
  }
  componentDidMount () {
    fetch(`/api/defaultTrans`).then(res => res.json())
    .then(json => {
      this.setState({
        right: json.value
      })
      this.getInterpreter(this.props.language.code, json.value)
    })
  }
  render () {
    const langCode = this.props.language.code
    return (
      <div style={styles.main} >
        <div style={styles.header} >
          <AppBar
            style={styles.appBar}
            title={(<div style={styles.title} >The CoolTE</div>)}
            iconElementLeft={<FQIcon onClick={() => this.props.history.push(`/${langCode}/help`)} />}
            iconElementRight={<IconButton><ActionHome /></IconButton>}
            onRightIconButtonTouchTap={() => this.props.history.push(`/${langCode}`)}
          />
          <div style={styles.code} >
            <div>FQ interpreter</div>
          </div>
        </div>
        <div style={styles.helpContent} >
          <div style={styles.head} >
            <SelectField style={styles.select} value={this.state.left} onChange={this.changeLeft} >
              <MenuItem value={'ko'} primaryText='한국어' />
              <MenuItem value={'en'} primaryText='English' />
              <MenuItem value={'es'} primaryText='Español' />
              <MenuItem value={'jp'} primaryText='日本' />
              <MenuItem value={'cs'} primaryText='简体中文' />
              <MenuItem value={'ct'} primaryText='繁体中文' />
              <MenuItem value={'it'} primaryText='italiano' />
              <MenuItem value={'de'} primaryText='Deutsch' />
              <MenuItem value={'ru'} primaryText='русский' />
              <MenuItem value={'fr'} primaryText='Français' />
              <MenuItem value={'th'} primaryText='ไทย' />
              <MenuItem value={'vn'} primaryText='Tiếng Việt' />
            </SelectField >
            <div style={styles.inter} ><span>to</span></div>
            <SelectField style={styles.select} value={this.state.right} onChange={this.changeRight} >
              <MenuItem value={'ko'} primaryText='한국어' />
              <MenuItem value={'en'} primaryText='English' />
              <MenuItem value={'es'} primaryText='Español' />
              <MenuItem value={'jp'} primaryText='日本' />
              <MenuItem value={'cs'} primaryText='简体中文' />
              <MenuItem value={'ct'} primaryText='繁体中文' />
              <MenuItem value={'it'} primaryText='italiano' />
              <MenuItem value={'de'} primaryText='Deutsch' />
              <MenuItem value={'ru'} primaryText='русский' />
              <MenuItem value={'fr'} primaryText='Français' />
              <MenuItem value={'th'} primaryText='ไทย' />
              <MenuItem value={'vn'} primaryText='Tiếng Việt' />
            </SelectField >
          </div>
          <Divider/>
          <div style={styles.sec} >
            {this.state.list.map((item, index) => {
              return (<InpterpreterCard index={index + 1} left={item[this.state.left]} right={item[this.state.right]} key={item._id}/>)
            })}
          </div>
        </div>
      </div>
    )
  }
}
function mapStateToProps (state) {
  const {
    language
  } = state
  return {language}
}
export default connect(mapStateToProps)(Help)
