export default {
  main: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: '100%',
    flexDirection: 'column',
    backgroundColor: '#e8efe1'
  },
  appBar: {
    backgroundColor: '#dc143c',
    boxShadow: 'none'
  },
  header: {
    position: 'fixed',
    top: '0',
    width: '100%',
    zIndex: 100
  },
  restaurant: {
  },
  code: {
    height: '2em',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#722F37',
    color: '#ffffff',
    boxShadow: 'rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px'
  },
  helpContent: {
    width: '100%',
    height: '100%',
    paddingTop: '100px',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    paddingBottom: '3.5em'
  },
  head: {
    display: 'flex',
    flexDirection: 'row'
  },
  inter: {
    paddingLeft: '2em',
    paddingRight: '2em',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    textAlign: 'center',
    color: '#33ccff',
    width: '100%'
  },
  select: {
    width: '130px'
  },
  sec: {
    width: '100%'
  }
}
