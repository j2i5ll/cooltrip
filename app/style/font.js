export default {
  preset1: {
    fontFamily: 'Jeju Gothic, sans-serif'
  },
  preset2: {
    fontFamily: 'Josefin Sans, sans-serif'
  },
  preset3: {
    fontFamily: 'Architects Daughter, cursive'
  },
  preset4: {
    fontFamily: 'Josefin Sans, sans-serif',
    fontWeight: 'bold'
  },
  preset5: {
    fontFamily: 'Libre Franklin, sans-serif'
  },
  preset6: {
    fontFamily: 'Roboto Slab, serif',
    fontWeight: 'bold'
  }
}
