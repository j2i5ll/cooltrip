import {
  REQUEST_RESTAURANT,
  RECEIVE_RESTAURANT,
  INIT_RESTAURANT
} from '../actions/actionType'
const initialState = {
  isFetching: false,
  data: {
    name: {
      origin: undefined,
      translate: {
        en: undefined
      }
    },
    menu: [],
    additionalInfo: ''
  }
/*
  isFetching: false,
  data: {
    code: 0,
    name: {
      origin: '레스토랑',
      translate: {
        en: 'restaurant'
      }
    },
    menu: [{
      id: 1230,
      name: {
        origin: '메뉴1',
        translate: {
          en: 'menu1'
        }
      },
      desc: {
        en: 'en description1'
      },
      caution: {
        en: 'en caution1'
      },
      price: [{
        order: 1,
        size: 'S',
        price: 9000
      }, {
        order: 2,
        size: 'M',
        price: 11000
      }, {
        order: 3,
        size: 'L',
        price: 13000
      }]
    }, {
      id: 1341,
      name: {
        origin: '메뉴2',
        translate: {
          en: 'menu2'
        }
      },
      desc: {
        en: 'en description2'
      },
      caution: undefined,
      price: 7000
    }, {
      id: 1441,
      name: {
        origin: '메뉴3',
        translate: {
          en: 'menu3'
        }
      },
      desc: {
        en: 'en description3'
      },
      caution: {
        en: 'en caution3'
      },
      price: [{
        order: 1,
        size: 'S',
        price: 19000
      }, {
        order: 2,
        size: 'M',
        price: 24000
      }, {
        order: 3,
        size: 'L',
        price: 31000
      }]
    }]
  }
*/
}

export default (state = initialState, action) => {
  switch (action.type) {
    case INIT_RESTAURANT:
      return {...initialState}
    case REQUEST_RESTAURANT:
      state.isFetching = true
      return {...state}
    case RECEIVE_RESTAURANT:
      state.isFetching = false
      if (action.err) {
        state.err = action.err
        return {...state}
      } else {
        state.data = action.data || state.data
        state.err = undefined
        return {...state}
      }
    default:
      return state
  }
}
