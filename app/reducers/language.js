import {CHANGE_LANGUAGE} from '../actions/actionType'
const initialState = {
  code: 'en'
}
export default (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_LANGUAGE:
      return {
        ...state,
        code: action.code
      }
    default:
      return state
  }
}
