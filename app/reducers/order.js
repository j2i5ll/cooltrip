// import {SET_CODE} from '../actions/actionType'
import {ADD_ORDER, REMOVE_ORDER, INIT_ORDER} from '../actions/actionType'
const initialState = {
  menu: []
/*
  menu: [{
    id: 1441,
    count: [{
      order: 1,
      size: 'S',
      count: 1
    }, {
      order: 2,
      size: 'L',
      count: 1
    }]
  }, {
    id: 1341,
    count: 2
  }]
*/
}
const findIndex = (state, id) => {
  for (let i = 0; i < state.menu.length; i++) {
    if (id === state.menu[i].id) {
      return i
    }
  }
  return undefined
}
/*
 * {
 *  id: string,
 *  size: string,
 *  order: number
 * }
 *
 */
export default (state = initialState, action) => {
  switch (action.type) {
    case INIT_ORDER:
      return {
        menu: []
      }
    case ADD_ORDER:
      const idx = findIndex(state, action.id)
      if (idx === undefined) {
        state.menu.push({
          id: action.id,
          count: action.size === undefined ? 1 : [{size: action.size, count: 1, order: action.order}]
        })
      } else {
        let count = state.menu[idx].count
        if (typeof count === 'number') {
          state.menu[idx].count++
        } else {
          for (let i = 0; i < state.menu[idx].count.length; i++) {
            if (state.menu[idx].count[i].size === action.size) {
              state.menu[idx].count[i].count++
              return {...state}
            }
          }
          state.menu[idx].count.push({
            size: action.size,
            count: 1,
            order: action.order
          })
          return {...state}
        }
      }
      return {...state}
    case REMOVE_ORDER:
      if (action.size === undefined) {    // only one menu
        if (state.menu[action.index].count === 1) {
          state.menu.splice(action.index, 1)
        } else {
          state.menu[action.index].count--
        }
      } else {
        const sizes = state.menu[action.index].count
        for (let i = 0; i < sizes.length; i++) {
          if (sizes[i].size === action.size) {
            state.menu[action.index].count[i].count--
            if (state.menu[action.index].count[i].count <= 0) {
              state.menu[action.index].count.splice(i, 1)
            }
            break
          }
        }
        if (state.menu[action.index].count.length === 0) {
          state.menu.splice(action.index, 1)
        }
      }
      return {...state}
    default:
      return state
  }
}
