import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import 'url-search-params-polyfill'
import thunkMiddleware from 'redux-thunk'

import AppRouter from './config/AppRouter'

import { createStore, combineReducers, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import createHistory from 'history/createBrowserHistory' // createBrowserHistory

import { routerReducer, routerMiddleware } from 'react-router-redux'

import restaurant from './reducers/restaurant'
import order from './reducers/order'
import language from './reducers/language'
import {changeLanguage} from './actions/language'

import I18n, {i18nState, setLanguage} from 'redux-i18n'
import translations from './i18n'
import langCode from './libs/LangCode'

const history = createHistory()
const middleware = routerMiddleware(history)
const store = createStore(
  combineReducers({
    restaurant,
    order,
    language,
    i18nState,
    router: routerReducer
  }),
  applyMiddleware(middleware, thunkMiddleware)
)

const render = (Component) => {
  ReactDOM.render(
   <AppContainer>
     <MuiThemeProvider>
        <Provider store={store}>
          <I18n translations={translations} initialLang="en" fallbackLang="en" >
            <Component history={history} />
          </I18n>
        </Provider>
      </MuiThemeProvider>
    </AppContainer>,
    document.getElementById('root')
  )
}

render(AppRouter)
const pathname = store.getState().router.location.pathname
const initLangCode = pathname.split('/')[1]
if (langCode[initLangCode]) {
  store.dispatch(changeLanguage(initLangCode))
  store.dispatch(setLanguage(initLangCode))
}

if (module.hot) {
  module.hot.accept('./config/AppRouter', () => {
    const newApp = require('./config/AppRouter').default
    render(newApp)
  })
}
