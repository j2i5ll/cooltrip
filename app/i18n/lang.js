const translationsObject = {
  en: {
    main: {
      title: 'Awesome app with i18n!',
      hello: 'Hello, %{name}!'
    }
  },
  ja: {
    main: {
      title: 'Toffe app met i18n!',
      hello: 'Hallo, %{name}!'
    }
  }
}

export default translationsObject
