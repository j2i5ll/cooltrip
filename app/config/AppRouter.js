import React, { Component } from 'react'
import { ConnectedRouter } from 'react-router-redux'
import { Switch, Route } from 'react-router-dom'
// import App from '../components/App'
import AppNavi from './AppNavi'
import LangCode from '../libs/LangCode'

class AppRouter extends Component {
  render () {
    const keys = Object.keys(LangCode)
    return (
      <ConnectedRouter history={this.props.history} >
        <Switch>
          <Route exact path='/' component={AppNavi} />
          {keys.map((c) => (<Route path={`/${c}`} component={AppNavi} key={c} />))}
        </Switch>
      </ConnectedRouter>
    )
  }
}
export default AppRouter
