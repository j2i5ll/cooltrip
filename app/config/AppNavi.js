import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
// import App from '../components/App'
import Main from '../screen/Main'
import Restaurant from '../screen/Restaurant'
import OrderResult from '../screen/OrderResult'
import Confirm from '../screen/Confirm'
import Bill from '../screen/Bill'
import Error from '../screen/Error'
import Help from '../screen/Help'

class AppRouter extends Component {
  render () {
    const {match} = this.props
    return (
      <Switch>
        <Route exact path={`${match.url}`} component={Main} />
        <Route path={`${match.url}/restaurant`} component={Restaurant} />
        <Route path={`${match.url}/orderResult`} component={OrderResult} />
        <Route path={`${match.url}/confirm`} component={Confirm} />
        <Route path={`${match.url}/bill`} component={Bill} />
        <Route path={`${match.url}/help`} component={Help} />
        <Route path={`${match.url}/error`} component={Error} />
      </Switch>
    )
  }
}
export default AppRouter
