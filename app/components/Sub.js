import React, { Component } from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as actions from '../actions/action'

class Sub extends Component {
  render () {
    console.log(this)
    return (
      <div>
        <p>Sub Page</p>
        <p>{this.props.counter.value}</p>
        <button onClick={this.props.increment} >+</button>
        <button onClick={this.props.decrement} >-</button>
        <button onClick={this.props.history.goBack} >Back</button>
      </div>
    )
  }
}
function mapStateToProps (state) {
  return {...state}
}
function mapDispatchToProps (dispatch) {
  return bindActionCreators(actions, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Sub)
