import React, { Component } from 'react'

class App extends Component {
  goSub () {
    this.props.history.push('/sub')
  }
  render () {
    console.log(this.props)
    console.log('hello')
    return (
      <div>
        <h2 id='heading'>Hello ReactJS</h2>
        <button onClick={this.goSub.bind(this)} >sub</button>
      </div>
    )
  }
}

export default App
