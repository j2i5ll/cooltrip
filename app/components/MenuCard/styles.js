export default {
  addItem: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingLeft: '1.5em'
  },
  menuItem: {
    maxWidth: '95%',
    width: '95%',
    marginBottom: '20px',
    backgroundColor: '#e8efe1'
  }
}
