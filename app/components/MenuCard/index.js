import React, { Component } from 'react'
import {Card, CardTitle, CardText} from 'material-ui/Card'
import IconButton from 'material-ui/IconButton'
import FontIcon from 'material-ui/FontIcon'
import {connect} from 'react-redux'
import styles from './styles'
import font from '../../style/font.js'
import {pink500, red500} from 'material-ui/styles/colors'
import Utils from '../../libs/Utils'
import * as orderActions from '../../actions/order'
import {bindActionCreators} from 'redux'
import Size from '../../libs/Size'

class MenuCard extends Component {
  constructor () {
    super()
    this.makePrice = this.makePrice.bind(this)
    this.addOrder = this.addOrder.bind(this)
  }
  addOrder (menuItem) {
    this.props.addOrder({
      id: this.props.menuItem.id,
      ...menuItem
    })
  }
  getLangTitle () {
    const menuItem = this.props.menuItem
		const {restaurant} = this.props
    const {defaultLang} = restaurant.data
    if (menuItem.name) {
      return defaultLang === 'ko' ? menuItem.name.origin : menuItem.name.translate[defaultLang]
    } else {
      return ''
    }
  }
  makePrice (price) {
    if (typeof price !== 'object') {
      return (
        <div style={styles.addItem} >
          <h3 style={font.preset1} >&#8361;{Utils.numberToPrice(price)}</h3>
          <div>
            <IconButton onClick={() => this.addOrder()} ><FontIcon className='material-icons' color={pink500} >add</FontIcon></IconButton>
          </div>
        </div>
      )
    } else {
      return price.map((p, index) => {
        return (
          <div style={styles.addItem} key={index} >
            <h3><span style={font.preset5} >{Size.get(p.size, this.props.language.code)}</span> - <span style={font.preset1} >&#8361;{Utils.numberToPrice(p.price)}</span></h3>
            <div>
              <IconButton onClick={() => this.addOrder(p)} ><FontIcon className='material-icons' color={pink500} >add</FontIcon></IconButton>
            </div>
          </div>
        )
      })
    }
  }
  render () {
    const menuItem = this.props.menuItem
    return (
      <Card style={styles.menuItem} >
        <CardTitle subtitle={<span style={font.preset5} >{menuItem.name.translate[this.props.language.code]}</span>} title={<span style={font.preset1} >{this.getLangTitle()}</span>} />
        <CardText>
          <p style={font.preset5} >{menuItem.desc[this.props.language.code]}</p>
          {this.makePrice(menuItem.price)}
          <p style={{...font.preset5, color: red500, paddingTop: '10px'}} >{menuItem.caution !== undefined ? menuItem.caution[this.props.language.code] : ''}</p>
        </CardText>
      </Card>
    )
  }
}
function mapStateToProps (state) {
  return {...state}
}
function mapDispatchToProps (dispatch) {
  return bindActionCreators({...orderActions}, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(MenuCard)
