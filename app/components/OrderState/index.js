import React, { Component } from 'react'
import styles from './styles.js'
import font from '../../style/font.js'
import FontIcon from 'material-ui/FontIcon'
import RaisedButton from 'material-ui/RaisedButton'
import TouchRipple from 'material-ui/internal/TouchRipple'
import * as restaurantActions from '../../actions/restaurant'
import * as orderActions from '../../actions/order'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import { withRouter } from 'react-router'
import OrderItem from '../OrderItem'
import Utils from '../../libs/Utils'

class OrderState extends Component {
  constructor () {
    super()
    this.state = {
      showContent: false
    }
    this.showOrders = this.showOrders.bind(this)
    this.order = this.order.bind(this)
    this.getOrderedMenu = this.getOrderedMenu.bind(this)
  }
  showOrders () {
    this.setState((oldState) => {
      return {
        showContent: !oldState.showContent
      }
    })
  }
  order (e) {
    const langCode = this.props.language.code
    document.body.style.overflow = ''
    this.props.history.push({
      pathname: `/${langCode}/orderResult`
    })
    e.stopPropagation()
    e.preventDefault()
  }
  getOrderedMenu () {
    return this.props.order.menu.map((item, index) => {
      return (<OrderItem orderedMenu={item} index={index} key={index} />)
    })
  }

  render () {
    // this.state.showContent
    const overlay = this.state.showContent ? (<div style={styles.overlay} onClick={this.showOrders}></div>) : null
    // const footerStyle = {...styles.footer, transform: (this.state.showContent ? 'translateY(0em)' : 'translateY(25em)')}
    const footerStyle = {...styles.footer, transform: (this.state.showContent ? 'translateY(0em)' : 'translateY(' + ((window.innerHeight / 2) - 45) + 'px)')}
    if (this.state.showContent) {
      document.body.style.overflow = 'hidden'
    } else {
      document.body.style.overflow = ''
    }
    const orderedMenus = this.props.order.menu

    let sumCount = 0
    let sumPrice = 0
    for (let i = 0; i < orderedMenus.length; i++) {
      const menuItem = this.props.restaurant.menu.find((item) => item.id === orderedMenus[i].id)
      if (typeof orderedMenus[i].count === 'number') {
        sumCount += orderedMenus[i].count
        sumPrice += (orderedMenus[i].count * menuItem.price)
      } else {
        for (let j = 0; j < orderedMenus[i].count.length; j++) {
          const itemPrice = menuItem.price.find((item) => item.size === orderedMenus[i].count[j].size)
          sumCount += orderedMenus[i].count[j].count
          sumPrice += (orderedMenus[i].count[j].count * itemPrice.price)
        }
      }
    }
    sumPrice = isNaN(sumPrice) ? 'ask to the server' : Utils.numberToPrice(sumPrice)

    return (
    <div>
      {overlay}
      <div style={footerStyle} >
        <div style={styles.headerContainer} onClick={this.showOrders} >
          <TouchRipple >
            <div style={styles.header} >
              <div>
                <FontIcon className='material-icons' style={styles.orderIcon} >{this.state.showContent ? 'close' : 'playlist_add_check'}</FontIcon>
              </div>
              <div style={styles.state} ><h4 style={font.preset1} >{sumCount} items / &#8361;{sumPrice}</h4></div>
            </div>
          </TouchRipple>
          <div >
            <RaisedButton labelStyle={font.preset6} label='Check' onClick={this.order} />
          </div>
        </div>
        <div style={{...styles.content, height: (window.innerHeight / 2) + 'px'}} >
          {this.getOrderedMenu()}
        </div>
      </div>
    </div>
    )
  }
}
function mapStateToProps (state) {
  const {
    language,
    order
  } = state
  return {
    language,
    order,
    restaurant: state.restaurant.data
  }
}
function mapDispatchToProps (dispatch) {
  return bindActionCreators({...restaurantActions, ...orderActions}, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(OrderState))
