export default {
  footer: {
    position: 'fixed',
    width: '100%',
    minHeight: '3.5em',
    bottom: 0,
    zIndex: 10,
    boxShadow: 'rgba(0, 0, 0, 0.12) 0px -2px 4px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
    transform: 'translateY(25em)',
    willChange: 'transform'
  },
  headerContainer: {
    position: 'absolute',
    top: 0,
    zIndex: 10,
    width: '100%',
    backgroundColor: '#722F37',
    color: '#ffffff',
    boxShadow: 'rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '3.6em',
    paddingLeft: '1.5em',
    paddingRight: '1.5em'
  },
  header: {
    display: 'flex',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: '3.5em'
  },
  orderIcon: {
    fontSize: '2em',
    color: '#ffffff'
  },
  content: {
    overflow: 'scroll',
    paddingTop: '3em',
    width: '100%',
    backgroundColor: '#e8efe1',
    marginTop: '10px',
    transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms'
    // height: '28em'
    // height: (window.innerHeight / 2) + 'px'
  },
  state: {
    flex: 1,
    paddingLeft: '1em'
  },
  overlay: {
    position: 'fixed',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    zIndex: 1
  }
}
// window.innerHeight > window.innerWidth
