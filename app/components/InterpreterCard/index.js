import React, { Component } from 'react'
import styles from './styles'
// import font from '../../style/font.js'

class InterpreterCard extends Component {
  constructor () {
    super()
    this.state = {
      open: false
    }
    this.toggleInterpreter = this.toggleInterpreter.bind(this)
    this.getTransferText = this.getTransferText.bind(this)
    this.getArrow = this.getArrow.bind(this)
  }
  toggleInterpreter () {
    this.setState((st) => {
      return {
        open: !st.open
      }
    })
  }
  getTransferText () {
    const {right} = this.props
    if (this.state.open) {
      return (
        <div style={styles.trans} >
          <div style={styles.transText} >{right.text}</div>
          <div style={styles.read} >{right.read}</div>
        </div>
      )
    } else {
      return null
    }
  }
  getArrow () {
    if (this.state.open) {
      return '▽'
    } else {
      return '▼'
    }
  }
  render () {
    const {left, index} = this.props
    return (
      <div style={styles.card} onClick={this.toggleInterpreter} >
        <h3 style={styles.number} >{index}. </h3>
        <div style={styles.content} >
          <div style={styles.originText} >
            <h3 style={styles.text} >{left.text}</h3>
            <div style={styles.arrow} >{this.getArrow()}</div>
          </div>
          {this.getTransferText()}
        </div>
      </div>
    )
  /*
    return (
      <Card style={styles.menuItem} >
        <CardTitle subtitle={<span style={font.preset5} >{menuItem.name.translate[this.props.language.code]}</span>} title={<span style={font.preset1} >{menuItem.name.origin}</span>} />
        <CardText>
          <p style={font.preset5} >{menuItem.desc[this.props.language.code]}</p>
          {this.makePrice(menuItem.price)}
          <p style={{...font.preset5, color: red500, paddingTop: '10px'}} >{menuItem.caution !== undefined ? menuItem.caution[this.props.language.code] : ''}</p>
        </CardText>
      </Card>
    )
    */
  }
}
export default InterpreterCard
