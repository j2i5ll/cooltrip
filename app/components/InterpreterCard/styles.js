export default {
  card: {
    borderTop: '1px solid #a9a9a9',
    width: '100%',
    padding: '1em 1em',
    display: 'flex',
    flexDirection: 'row'
  },
  content: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column'
  },
  originText: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  text: {
    wordBreak: 'break-all',
    flex: 1
  },
  trans: {
    display: 'flex',
    flexDirection: 'column',
    paddingTop: '10px'
  },
  transText: {
    wordBreak: 'break-all',
    fontSize: '2em'
  },
  number: {
    paddingRight: '1em'
  },
  read: {
    wordBreak: 'break-all'
  }
}
