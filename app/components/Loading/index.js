import React, { Component } from 'react'
import styles from './styles.js'
import LinearProgress from 'material-ui/LinearProgress'
import { withRouter } from 'react-router'
import {pink500} from 'material-ui/styles/colors'
import {connect} from 'react-redux'

class Loading extends Component {
  constructor () {
    super()
    this.state = {
      count: 2
    }
    this.timer = setInterval(this.updateTimer.bind(this), 1000)
  }
  updateTimer () {
    if (this.state.count > 0) {
      this.setState((oldState) => {
        return {
          count: oldState.count - 1
        }
      })
    }
  }
  render () {
    const {restaurant, history} = this.props
    const {isFetching, err} = restaurant
    const langCode = this.props.language.code
    if (this.state.count <= 0 && isFetching === false) {
      clearInterval(this.timer)
      if (err || restaurant.data.menu.length === 0) {
        history.replace(`/${langCode}/error`)
      }
      return null
    } else {
      return (
        <div style={styles.loading}>
          <LinearProgress mode="indeterminate" style={{borderRadius: 0}} color={pink500}/>
          <div style={styles.ad} >
            <h1>AD Area</h1>
          </div>
          <div style={styles.timer}>
            <h4>Loading ... {this.state.count === 0 ? 'wait a second' : this.state.count}</h4>
          </div>
        </div>
      )
    }
  }
}
function mapStateToProps (state) {
  return {
    restaurant: state.restaurant,
    language: state.language
  }
}
export default connect(mapStateToProps)(withRouter(Loading))
