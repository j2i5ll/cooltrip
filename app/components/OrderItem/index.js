import React, { Component } from 'react'
import * as orderActions from '../../actions/order'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {List, ListItem} from 'material-ui/List'
import Divider from 'material-ui/Divider'
import styles from './styles'
import IconButton from 'material-ui/IconButton'
import FontIcon from 'material-ui/FontIcon'
import {pink500} from 'material-ui/styles/colors'
import Utils from '../../libs/Utils'
import Size from '../../libs/Size'
import font from '../../style/font'

class OrderItem extends Component {
  constructor () {
    super()
    this.addOrder = this.addOrder.bind(this)
    this.removeOrder = this.removeOrder.bind(this)
  }
  addOrder (orderItem) {
    this.props.addOrder({
      id: this.props.orderedMenu.id,
      ...orderItem
    })
  }
  removeOrder (size) {
    this.props.removeOrder({
      index: this.props.index,
      size: size
    })
  }
  getPrices (selectedMenuInfo) {
    const orderedMenu = this.props.orderedMenu
    if (typeof orderedMenu.count === 'number') {    // only one size
      return (<ListItem insetChildren={true} primaryText={
        <div style={{...styles.price, ...font.preset5}} >
          <div>&#8361;{Utils.numberToPrice(selectedMenuInfo.price)} x {orderedMenu.count}</div>
          <div style={styles.btns} >
            <IconButton onClick={() => this.addOrder()} ><FontIcon className='material-icons' color={pink500} >add</FontIcon></IconButton>
            <IconButton onClick={() => this.removeOrder()}><FontIcon className='material-icons' color={pink500} >remove</FontIcon></IconButton>
          </div>
        </div>
      } />)
    } else {    // has various size
      orderedMenu.count.sort((a, b) => {
        return a.order - b.order
      })
      return orderedMenu.count.map((orderItem, index) => {
        const onePrice = selectedMenuInfo.price.find((p) => p.size === orderItem.size)
        return (<ListItem insetChildren={true} key={index} primaryText={
          <div style={{...styles.price, ...font.preset5}} >
            <div>{Size.get(orderItem.size, this.props.language.code)} - &#8361;{Utils.numberToPrice(onePrice.price)} x {orderItem.count}</div>
            <div style={styles.btns} >
              <IconButton onClick={() => this.addOrder(orderItem)} ><FontIcon className='material-icons' color={pink500} >add</FontIcon></IconButton>
              <IconButton onClick={() => this.removeOrder(orderItem.size)}><FontIcon className='material-icons' color={pink500} >remove</FontIcon></IconButton>
            </div>
          </div>
        } />)
      })
    }
  }
  componentWillMount () {
    ListItem.defaultProps.disableTouchRipple = true
    ListItem.defaultProps.disableFocusRipple = true
  }
  render () {
    const selectedMenuInfo = this.props.restaurant.menu.find((menuItem) => menuItem.id === this.props.orderedMenu.id)
    const name = selectedMenuInfo.name.translate[this.props.language.code]
    const desc = selectedMenuInfo.desc[this.props.language.code]
    const price = this.getPrices(selectedMenuInfo)
    return (
    <div>
      <List>
        <ListItem primaryText={<div style={font.preset5} >{name}</div>} secondaryText={<div style={font.preset5} >{desc}</div>} />
        {price}
      </List>
      <Divider />
    </div>
    )
  }
}
function mapStateToProps (state) {
  const {
    language,
    order
  } = state
  return {
    language,
    order,
    restaurant: state.restaurant.data
  }
}
function mapDispatchToProps (dispatch) {
  return bindActionCreators({...orderActions}, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(OrderItem)
