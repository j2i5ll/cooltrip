export default {
  footer: {
    position: 'fixed',
    bottom: 0,
    width: '100%',
    height: '3.5em',
    backgroundColor: '#722f37',
    color: 'white',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center'
  },
  copyright: {
    fontSize: '80%'
  },
  mail: {
    fontSize: '80%',
    textDecoration: 'underline'
  }
}
