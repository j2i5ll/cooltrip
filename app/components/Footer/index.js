import React, { Component } from 'react'
import styles from './styles.js'

class Footer extends Component {
  render () {
    return (
      <div style={styles.footer}>
        <div style={styles.copyright} >&#9400;2017  The coolest trip ever</div>
        <a href='mailto:thecoolte@gmail.com' style={styles.mail} >thecoolte@gmail.com</a>
      </div>
    )
  }
}
export default Footer
