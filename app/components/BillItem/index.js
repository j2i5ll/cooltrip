import React, { Component } from 'react'
import * as orderActions from '../../actions/order'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {List, ListItem} from 'material-ui/List'
import Divider from 'material-ui/Divider'
import styles from './styles'
import Utils from '../../libs/Utils'
import Size from '../../libs/Size'

class OrderItem extends Component {
  constructor () {
    super()
    this.addOrder = this.addOrder.bind(this)
    this.removeOrder = this.removeOrder.bind(this)
  }
  addOrder (orderItem) {
    this.props.addOrder({
      id: this.props.orderedMenu.id,
      ...orderItem
    })
  }
  removeOrder (size) {
    this.props.removeOrder({
      index: this.props.index,
      size: size
    })
  }
  getPrices (selectedMenuInfo) {
    const orderedMenu = this.props.orderedMenu
    if (typeof orderedMenu.count === 'number') {    // only one size
      return (<ListItem insetChildren={true} primaryText={
        <div style={styles.price} >
          <div>&#8361;{Utils.numberToPrice(selectedMenuInfo.price)}</div>
          <div> x {orderedMenu.count}</div>
        </div>
      } />)
    } else {    // has various size
      orderedMenu.count.sort((a, b) => {
        return a.order - b.order
      })
      return orderedMenu.count.map((orderItem, index) => {
        const onePrice = selectedMenuInfo.price.find((p) => p.size === orderItem.size)
        return (<ListItem insetChildren={true} key={index} primaryText={
          <div style={styles.price} >
            <div>{Size.get(orderItem.size, this.props.language.code)} - &#8361;{Utils.numberToPrice(onePrice.price)}</div>
            <div> x {orderItem.count}</div>
          </div>
        } />)
      })
    }
  }
  componentWillMount () {
    ListItem.defaultProps.disableTouchRipple = true
    ListItem.defaultProps.disableFocusRipple = true
  }
  render () {
    const selectedMenuInfo = this.props.restaurant.menu.find((menuItem) => menuItem.id === this.props.orderedMenu.id)
    const name = selectedMenuInfo.name.translate[this.props.language.code]
    const desc = selectedMenuInfo.desc[this.props.language.code]
    const price = this.getPrices(selectedMenuInfo)
    return (
    <div>
      <List>
        <ListItem primaryText={name} secondaryText={desc} />
        {price}
      </List>
      <Divider />
    </div>
    )
  }
}
function mapStateToProps (state) {
  const {
    order,
    language
  } = state
  return {
    order,
    language,
    restaurant: state.restaurant.data
  }
}
function mapDispatchToProps (dispatch) {
  return bindActionCreators({...orderActions}, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(OrderItem)
