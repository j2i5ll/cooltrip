import React, { Component } from 'react'
import * as orderActions from '../../actions/order'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {List, ListItem} from 'material-ui/List'
import Divider from 'material-ui/Divider'
import styles from './styles'
import Utils from '../../libs/Utils'
import Size from '../../libs/Size'
import font from '../../style/font'

class OrderItem extends Component {
  constructor () {
    super()
    this.addOrder = this.addOrder.bind(this)
    this.removeOrder = this.removeOrder.bind(this)
  }
  getLangTitle () {
  }
  addOrder (orderItem) {
    this.props.addOrder({
      id: this.props.orderedMenu.id,
      ...orderItem
    })
  }
  removeOrder (size) {
    this.props.removeOrder({
      index: this.props.index,
      size: size
    })
  }
  getPrices (selectedMenuInfo) {
    const orderedMenu = this.props.orderedMenu
    if (typeof orderedMenu.count === 'number') {    // only one size
      return (<ListItem insetChildren={true} primaryText={
        <div style={{...font.preset1, ...styles.price}} >
          <div>&#8361;{Utils.numberToPrice(selectedMenuInfo.price)}</div>
          <div> x {orderedMenu.count}</div>
        </div>
      } />)
    } else {    // has various size
      orderedMenu.count.sort((a, b) => {
        return a.order - b.order
      })
      return orderedMenu.count.map((orderItem, index) => {
        const onePrice = selectedMenuInfo.price.find((p) => p.size === orderItem.size)
        return (<ListItem insetChildren={true} key={index} primaryText={
          <div style={{...font.preset1, ...styles.price}} >
            <div>{Size.get(orderItem.size, 'ko')} - &#8361;{Utils.numberToPrice(onePrice.price)}</div>
            <div> x {orderItem.count}</div>
          </div>
        } />)
      })
    }
  }
  componentWillMount () {
    ListItem.defaultProps.disableTouchRipple = true
    ListItem.defaultProps.disableFocusRipple = true
  }
  render () {
    const selectedMenuInfo = this.props.restaurant.menu.find((menuItem) => menuItem.id === this.props.orderedMenu.id)
    // const name = selectedMenuInfo.name.origin

    const restaurant = this.props.restaurant
    const {defaultLang} = restaurant
    const name =  defaultLang === 'ko' ? selectedMenuInfo.name.origin : selectedMenuInfo.name.translate[defaultLang]

    const price = this.getPrices(selectedMenuInfo)
    return (
    <div>
      <List>
        <ListItem primaryText={<span style={font.preset1} >{name}</span>} />
        {price}
      </List>
      <Divider />
    </div>
    )
  }
}
function mapStateToProps (state) {
  const {
    order
  } = state
  return {
    order,
    restaurant: state.restaurant.data
  }
}
function mapDispatchToProps (dispatch) {
  return bindActionCreators({...orderActions}, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(OrderItem)
