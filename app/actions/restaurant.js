import fetch from 'cross-fetch'
import {
  REQUEST_RESTAURANT,
  RECEIVE_RESTAURANT,
  INIT_RESTAURANT
} from './actionType'

export function initRestaurant () {
  return {
    type: INIT_RESTAURANT
  }
}

export function requestRestaurant () {
  return {
    type: REQUEST_RESTAURANT
  }
}

export function fetchRestaurant (code) {
  return (dispatch, getState) => {
    if (getState().restaurant.isFetching) {
      return
    }
    dispatch(requestRestaurant())
    return fetch('/api/restaurant/' + code)
    .then(res => res.json())
    .then(json => dispatch(receiveRestaurant(json)))
    .catch((err) => dispatch(receiveRestaurant(null, err)))
  }
}
export function receiveRestaurant (data, err) {
  return {
    type: RECEIVE_RESTAURANT,
    data,
    err
  }
}
