import {CHANGE_LANGUAGE} from './actionType'

export function changeLanguage (code) {
  return {
    type: CHANGE_LANGUAGE,
    code
  }
}
