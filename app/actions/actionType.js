export const INIT_ORDER = 'initOrder'
export const ADD_ORDER = 'addOrder'
export const REMOVE_ORDER = 'removeOrder'

export const CHANGE_LANGUAGE = 'changeLang'

export const REQUEST_RESTAURANT = 'requestRestaurant'
export const RECEIVE_RESTAURANT = 'receiveRestaurant'

export const INIT_RESTAURANT = 'initRestaurant'
