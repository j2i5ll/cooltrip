import {ADD_ORDER, REMOVE_ORDER, INIT_ORDER} from './actionType'
// import {REMOVE_MENU} from './actionType'

/* orderInfo
 * {
 *   idx: number,
 *   size: string,
 *   order: number
 * }
 */
export function addOrder (orderInfo) {
  return {
    type: ADD_ORDER,
    ...orderInfo
  }
}

/* orderInfo
 * {
 *  index: number,
 *  size : string,
 * }
 *
 */
export function removeOrder (orderInfo) {
  return {
    type: REMOVE_ORDER,
    ...orderInfo
  }
}

export function initOrder () {
  return {
    type: INIT_ORDER
  }
}
