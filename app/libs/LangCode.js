const langCode = {
  'en': {
    name: 'English'
  },
  'es': {
    name: 'Español'
  },
  'jp': {
    name: '日本'
  },
  'cs': {
    name: '简体中文'
  },
  'ct': {
    name: '繁体中文'
  },
  'it': {
    name: 'italiano'
  },
  'de': {
    name: 'Deutsch'
  },
  'ru': {
    name: 'русский'
  },
  'fr': {
    name: 'Français'
  },
  'th': {
    name: 'ไทย'
  },
  'vn': {
    name: 'Tiếng Việt'
  }
}
export default langCode
