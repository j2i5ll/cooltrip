class Size {
  constructor () {
    this.size = {
      'S': {
        'en': 'Small',
        'ko': '소'
      },
      'M': {
        'en': 'Medium',
        'ko': '중'
      },
      'L': {
        'en': 'Large',
        'ko': '대'
      },
      'XL': {
        'en': 'Extra Large',
        'ko': '특대'
      },
      'R': {
        'en': 'Regular',
        'ko': '보통'
      },
      'F': {
        'en': 'Family',
        'ko': '패밀리'
      },
      'P': {
        'en': 'Party',
        'ko': '파티'
      },
      'GT': {
        'en': 'Good for two',
        'ko': '2인분'
      },
      'GTT': {
        'en': 'Good for two or three',
        'ko': '2~3인분'
      },
      'ACI': {
        'en': 'A cup(Iced)',
        'ko': '한잔(아이스)'
      },
      'ACH': {
        'en': 'A cup(Hot)',
        'ko': '한잔'
      },
      'CB100': {
        'en': 'Coffee beans 100g',
        'ko': '커피빈 100g'
      }
    }
  }
  set (size) {
    this.size = size
  }
  get (code, lang) {
    return this.size[code]['en']
  }
}
export default new Size()
