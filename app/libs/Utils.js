class Utils {
  numberToPrice (num) {
    return (num + '').toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  }
}
export default new Utils()
